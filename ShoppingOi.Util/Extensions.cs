﻿using RafaelPertence.ERP.Util;

namespace ShoppingOi.Util
{
    public static class Extensions
    {
        public static uint Descriptografar(this string valor)
        {
            if (string.IsNullOrEmpty(valor)) return 0;
            string descript = Criptografia.Descriptografar(valor);

            return uint.TryParse(descript, out uint codigo) ? codigo : 0;
        }

        public static string Criptografar(this uint codigo)
        {
            return codigo.ToString().Criptografar();
        }
        
        public static string Criptografar(this string valor)
        {
            return Criptografia.Criptografar(valor);
        }
    }
}
