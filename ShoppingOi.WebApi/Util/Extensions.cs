﻿using RafaelPertence.ERP.Util;
using ShoppingOi.DTO;
using ShoppingOi.Model;
using ShoppingOi.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingOi.WebApi.Util
{
    public static class Extensions
    {
        public static CategoriaApp ToApp(this Categoria model)
        {
            return new CategoriaApp
            {
                Codigo = model.Codigo.Criptografar(),
                Descricao = model.Descricao
            };
        }

        public static Categoria ToModel(this CategoriaApp app)
        {
            return new Categoria
            {
                Codigo = app.Codigo.Descriptografar(),
                Descricao = app.Descricao
            };
        }

        public static Erro ToModel(this ErroApp app)
        {
            return new Erro
            {
                Mensagem = app.Mensagem,
                Local = app.Local,
                Data = DateTime.Now
            };
        }

        public static UsuarioApp ToApp(this Usuario model)
        {
            return new UsuarioApp
            {
                Codigo = model.Codigo.Criptografar(),
                Nome = model.Nome,
                Email = model.Email,
                Ativo = model.Ativo,
                CodigoLoja = model.Loja?.Codigo.Criptografar()
            };
        }

        public static Usuario ToModel(this UsuarioApp app)
        {
            return new Usuario
            {
                Codigo = app.Codigo.Descriptografar(),
                Nome = app.Nome,
                Email = app.Email,
                Senha = app.Senha.Criptografar(),
                DataCadastro = app.DataCadastro
            };
        }

        public static LojaApp ToApp(this Loja model)
        {
            return new LojaApp
            {
                Codigo = model.Codigo.Criptografar(),
                Nome = model.Nome,
                Descricao = model.Descricao,
                Localizacao = model.Localizacao,
                Categoria = model.Categoria
            };
        }

        public static Loja ToModel(this LojaApp app)
        {
            return new Loja
            {
                Codigo = app.Codigo.Descriptografar(),
                CodigoUsuario = app.CodigoUsuario.Descriptografar(),
                Nome = app.Nome,
                Descricao = app.Descricao,
                Localizacao = app.Localizacao,
                Categorias = app.Categorias.Select(c => c.ToModel()).ToList()
            };
        }

        public static LojaCategoriaApp ToApp(this LojaCategoria model)
        {
            return new LojaCategoriaApp
            {
                CodigoCategoria = model.CodigoCategoria.Criptografar(),
                CodigoLoja = model.CodigoLoja.Criptografar()
            };
        }
    }
}