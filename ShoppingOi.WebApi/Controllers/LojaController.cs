﻿using RafaelPertence.ERP.Util;
using ShoppingOi.DTO;
using ShoppingOi.Model;
using ShoppingOi.Util;
using ShoppingOi.WebApi.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ShoppingOi.WebApi.Controllers
{
    public class LojaController : BaseController
    {
        [HttpGet]
        public BaseApiClass Get()
        {
            try
            {
                var lojas = Loja.GetRepository().BuscaListaLojas();
                return new LojaAppCollection
                {
                    Sucesso = true,
                    Lojas = lojas.Select(l => new LojaApp
                    {
                        Codigo = l.Codigo.Criptografar(),
                        Nome = l.Nome,
                        Categoria = l.Categoria
                    })
                };
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiLojaGet", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }

        [HttpGet]
        public BaseApiClass Get(string id)
        {
            try
            {
                var loja = Loja.GetById(Unescape(id).Descriptografar());
                var lojaApp = loja.ToApp();
                lojaApp.LojasCategorias = loja.LojasCategorias.Select(lc => lc.ToApp()).ToList();

                return lojaApp;
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiLojaGetItem", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }

        [HttpPost]
        public BaseApiClass Post([FromBody]LojaApp item)
        {
            try
            {
                var model = item.ToModel();
                model.Salvar();

                item.Codigo = model.Codigo.Criptografar();
                return item;
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiLojaPost", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }
    }
}
