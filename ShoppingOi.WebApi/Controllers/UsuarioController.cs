﻿using RafaelPertence.ERP.Util;
using ShoppingOi.DTO;
using ShoppingOi.Model;
using ShoppingOi.Util;
using ShoppingOi.WebApi.Util;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace ShoppingOi.WebApi.Controllers
{
    public class UsuarioController : ApiController
    {
        private const string TITULO_VALIDACAO_CADASTRO = "Validação de Cadastro";
        private const string TITULO_REDEFINIR_SENHA = "Redefinição de Senha";
        private const string MENSAGEM_VALIDACAO_CADASTRO = "Informe o código abaixo no aplicativo para confirmar seu cadastro:<br /><br />{0}.";
        private const string MENSAGEM_REDEFINIR_SENHA = "Informe o código abaixo no aplicativo para redefinir sua senha:<br /><br />{0}<br /><br />Se você não solicitou redefinição de senha, ignore esse e-mail.";

        //public BaseApiClass Get(string id)
        //{
        //    try
        //    {
        //        var usuario = Usuario.GetById(id.Descriptografar()).ToApp();
        //        usuario.Sucesso = true;

        //        return usuario;
        //    }
        //    catch (Exception ex)
        //    {
        //        Erro.Inserir("ApiUsuarioGet", ex.ToString());
        //        return BaseApiClass.MontaErro(ex.Message);
        //    }
        //}

        [HttpGet]
        [Route("api/usuario/login")]
        public BaseApiClass Login(string email, string senha)
        {
            try
            {
                Filtro filtro = new Filtro
                {
                    Filtros = new List<Filtro>
                    {
                        Filtro.FiltroStringIgual("Email", email),
                        Filtro.FiltroStringIgual("Senha", senha.Criptografar())
                    }
                };

                var usuario = Usuario.GetRepository().BuscaItem(filtro);

                if (usuario != null)
                {
                    if (!usuario.Ativo)
                        return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.CadastroNaoConfirmado, null);

                    var usuarioApp = usuario.ToApp();
                    usuarioApp.Sucesso = true;
                    return usuarioApp;
                }
                else
                    return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.DadosNaoEncontrados, "Dados não encontrados", true);
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioLogin", ex.ToString());
                return BaseApiClass.MontaErro(0, ex.Message, true);
            }
        }

        [HttpGet]
        [Route("api/usuario/enviarcodigoredefinicaosenha")]
        public BaseApiClass EnviarCodigoRedefinicaoSenha(string email)
        {
            try
            {
                var usuario = Usuario.GetRepository().BuscaItem(Filtro.FiltroStringIgual("Email", email));

                if (usuario == null)
                    return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.DadosNaoEncontrados, "Dados não encontrados", true);
                else
                {
                    usuario.CodigoValidacao = new Random((int)DateTime.Now.Millisecond).Next(100000, 999999).ToString();
                    usuario.DataValidadeCodigo = DateTime.Now.AddMinutes(10);
                    usuario.Salvar();

                    string msgEmail = string.Format(MENSAGEM_REDEFINIR_SENHA, usuario.CodigoValidacao);

                    var retornoEmail = Email.EnviaEmail(email,
                        TITULO_REDEFINIR_SENHA,
                        msgEmail);

                    if (!retornoEmail.Sucesso)
                    {
                        Erro.Inserir("ApiUsuarioEnviarCodigoRedefinicaoSenha", retornoEmail.Mensagem);
                        return BaseApiClass.MontaErro(0, "Erro ao enviar o e-mail", true);
                    }
                    else
                        return BaseApiClass.BaseSucesso;
                }
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioEnviarCodigoRedefinicaoSenha", ex.ToString());
                return BaseApiClass.MontaErro(0, ex.Message, true);
            }
        }

        [HttpGet]
        [Route("api/usuario/confirmarcodigo")]
        public BaseApiClass ConfirmarCodigo(string email, string codigoConfirmacao)
        {
            try
            {
                var usuario = Usuario.GetRepository().BuscaItem(Filtro.FiltroStringIgual("Email", email));

                if (usuario == null)
                    return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.DadosNaoEncontrados, "Dados não encontrados", true);
                else
                {
                    if (!usuario.CodigoValidacao.Equals(codigoConfirmacao))
                        return BaseApiClass.MontaErro("Código incorreto!");
                    else
                    {
                        if (usuario.DataValidadeCodigo < DateTime.Now)
                            return BaseApiClass.MontaErro("Validade do código expirou!");
                        else
                        {
                            usuario.Ativo = true;
                            usuario.Salvar();

                            var app = usuario.ToApp();
                            app.Sucesso = true;
                            return app;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioEnviarCodigoRedefinicaoSenha", ex.ToString());
                return BaseApiClass.MontaErro(0, ex.Message, true);
            }
        }

        [HttpPut]
        [Route("api/usuario/alterarsenha")]
        public BaseApiClass AlterarSenha([FromBody]UsuarioApp item)
        {
            try
            {
                var usuario = Usuario.GetRepository().BuscaItem(Filtro.FiltroStringIgual("Email", item.Email));

                if (usuario == null)
                    return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.DadosNaoEncontrados, "Dados não encontrados", true);
                else
                {
                    usuario.Senha = item.Senha.Criptografar();
                    usuario.Salvar();

                    return BaseApiClass.BaseSucesso;
                }
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioAlterarSenha", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }

        [HttpPost]
        public BaseApiClass Post([FromBody]UsuarioApp item)
        {
            try
            {
                if (Usuario.ExisteEmail(item.Email))
                    return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.EmailJaCadastrado, "Esse e-mail já está cadastrado na base de dados.");

                Usuario usuario = item.ToModel();
                usuario.DataCadastro = DateTime.Now;
                usuario.Ativo = false;
                usuario.CodigoValidacao = new Random((int)DateTime.Now.Millisecond).Next(100000, 999999).ToString();
                usuario.DataValidadeCodigo = DateTime.Now.AddMinutes(10);
                usuario.Salvar();

                string msgEmail = string.Format(MENSAGEM_VALIDACAO_CADASTRO, usuario.CodigoValidacao);

                var retornoEmail = Email.EnviaEmail(usuario.Email,
                    TITULO_VALIDACAO_CADASTRO,
                    msgEmail);

                //if (!retornoEmail.Sucesso)
                //{
                //    Erro.Inserir("ApiUsuarioPost", retornoEmail.Mensagem);
                //    return BaseApiClass.MontaErro(0, "Erro ao enviar o e-mail de confirmação", true);
                //}

                item.Sucesso = true;
                item.Codigo = usuario.Codigo.Criptografar();

                return item;
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioPost", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }

        [HttpPut]
        public BaseApiClass Put(string id, [FromBody]UsuarioApp item)
        {
            try
            {
                Usuario usuario = Usuario.GetById(item.Codigo.Descriptografar());

                if (Usuario.ExisteEmail(item.Email, usuario.Codigo))
                    return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.EmailJaCadastrado, "Esse e-mail já está cadastrado na base de dados.");

                usuario.Nome = item.Nome;
                usuario.Email = item.Email;

                usuario.Salvar();

                return BaseApiClass.BaseSucesso;
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioPut", ex.ToString());
                return BaseApiClass.MontaErro(0, ex.Message, true);
            }
        }

        [HttpPost]
        [Route("api/usuario/reenviarcodigo")]
        public BaseApiClass ReenviarCodigo(string email, int tipoCodigo)
        {
            try
            {
                var usuario = Usuario.GetRepository().BuscaItem(Filtro.FiltroStringIgual("Email", email));

                usuario.CodigoValidacao = new Random((int)DateTime.Now.Millisecond).Next(100000, 999999).ToString();
                usuario.DataValidadeCodigo = DateTime.Now.AddMinutes(10);
                usuario.Salvar();

                string msgEmail = string.Format(tipoCodigo == 1 ? MENSAGEM_VALIDACAO_CADASTRO : MENSAGEM_REDEFINIR_SENHA, usuario.CodigoValidacao);

                var retornoEmail = Email.EnviaEmail(email,
                    tipoCodigo == 1 ? TITULO_VALIDACAO_CADASTRO : TITULO_REDEFINIR_SENHA,
                    msgEmail);

                if (!retornoEmail.Sucesso)
                {
                    Erro.Inserir("ApiUsuarioReenviarCodigo", retornoEmail.Mensagem);
                    return BaseApiClass.MontaErro(0, "Erro ao enviar o e-mail com o código", true);
                }
                else
                    return BaseApiClass.BaseSucesso;
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiUsuarioPost", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }
    }
}
