﻿using RafaelPertence.ERP.Util;
using ShoppingOi.DTO;
using ShoppingOi.Model;
using ShoppingOi.WebApi.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ShoppingOi.WebApi.Controllers
{
    public class CategoriaController : ApiController
    {
        [HttpGet]
        public BaseApiClass Get()
        {
            try
            {
                var categorias = Categoria.GetRepository().BuscaLista(new Filtro("Ativo", Filtro.TipoFiltro.igual, System.Data.DbType.Boolean, true));
                return new CategoriaAppCollection
                {
                    Categorias = categorias.Select(c => c.ToApp())
                };
            }
            catch (Exception ex)
            {
                Erro.Inserir("ApiCategoriaGet", ex.ToString());
                return BaseApiClass.MontaErro(ex.Message);
            }
        }
    }
}
