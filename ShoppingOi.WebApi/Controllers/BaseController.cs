﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ShoppingOi.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        protected string Unescape(string valor)
        {
            return Uri.UnescapeDataString(valor);
        }
    }
}