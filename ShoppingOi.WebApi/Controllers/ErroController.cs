﻿using ShoppingOi.DTO;
using ShoppingOi.WebApi.Util;
using System.Web.Http;

namespace LikeImoveis.WebApi.Controllers
{
    public class ErroController : ApiController
    {
        [HttpPost]
        public void Post([FromBody]ErroApp item)
        {
            item.ToModel().Inserir();
        }
    }
}
