﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading;
using ShoppingOi.Service;
using ShoppingOi.App.Droid.Util;
using System.Threading.Tasks;
using System.IO;
using Android.Content;

namespace ShoppingOi.App.Droid {
    [Activity(
        Label = "ShoppingOi.App",
        Icon = "@drawable/icon",
        Theme = "@style/MainTheme",
        MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait,
        WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity {
        public static MainActivity Current;
        public static readonly int PickImageId = 1000;
        public TaskCompletionSource<Stream> PickImageTaskCompletionSource { set; get; }

        protected override void OnCreate(Bundle bundle) {
            AndroidEnvironment.UnhandledExceptionRaiser += AndroidEnvironment_UnhandledExceptionRaiser;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new ShoppingOi.App.App());

            MainActivity.Current = this;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent intent) {
            base.OnActivityResult(requestCode, resultCode, intent);

            if (requestCode == PickImageId) {
                if ((resultCode == Result.Ok) && (intent != null)) {
                    Android.Net.Uri uri = intent.Data;
                    Stream stream = ContentResolver.OpenInputStream(uri);

                    // Set the Stream as the completion of the Task
                    PickImageTaskCompletionSource.SetResult(stream);
                }
                else {
                    PickImageTaskCompletionSource.SetResult(null);
                }
            }
        }

        private void AndroidEnvironment_UnhandledExceptionRaiser(object sender, RaiseThrowableEventArgs e) {
            new ErroService(new Conexao()).Inserir(new DTO.ErroApp {
                Local = "Geral",
                Mensagem = e.Exception.ToString()
            });

            string mensagem;

#if DEBUG
            mensagem = e.Exception.Message;
#else
            mensagem = e.Exception.Message;
#endif


            Toast.MakeText(this, "Ocorreu um erro inesperado!", ToastLength.Long).Show();

            e.Handled = true;
        }
    }
}

