﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ShoppingOi.App.Droid.CustomRenderer;
using ShoppingOi.App.Droid.Util;

[assembly: ExportRenderer(typeof(Button), typeof(SOButtonRenderer))]
namespace ShoppingOi.App.Droid.CustomRenderer
{
    public class SOButtonRenderer : ButtonRenderer
    {
        public SOButtonRenderer(Android.Content.Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Typeface = FontManager.Instancia(MainActivity.Current.Assets).FonteBase;
                //Control.SetBackgroundResource(Resource.Drawable.Botao);
                //Control.SetTextColor(Resources.GetColor(Resource.Color.CinzaClaro));
            }
        }
    }
}