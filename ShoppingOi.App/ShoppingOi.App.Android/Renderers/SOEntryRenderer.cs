﻿using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using ShoppingOi.App.Droid.CustomRenderer;
using ShoppingOi.App.Droid.Util;
using Android.Views;

[assembly: ExportRenderer(typeof(Entry), typeof(SOEntryRenderer))]
namespace ShoppingOi.App.Droid.CustomRenderer
{
    public class SOEntryRenderer : EntryRenderer
    {
        public SOEntryRenderer(global::Android.Content.Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetBackgroundResource(Resource.Drawable.Entry);
                Control.Typeface = FontManager.Instancia(MainActivity.Current.ApplicationContext.Assets).FonteBase;
                //Control.TextAlignment = global::Android.Views.TextAlignment.Center;
                //Control.Gravity = GravityFlags.Center | GravityFlags.Left;
                //Control.Ellipsize = global::Android.Text.TextUtils.TruncateAt.Start;
                Control.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

                var padding = Resources.GetInteger(Resource.Integer.EntryPadding);
                Control.SetPadding(padding, padding, padding, padding);
            }
        }
    }
}