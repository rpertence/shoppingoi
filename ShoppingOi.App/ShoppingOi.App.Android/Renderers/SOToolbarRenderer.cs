﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android.AppCompat;
using Android.Support.V7.Widget;
using ShoppingOi.App.Droid.Renderers;
using ShoppingOi.App.Droid.Util;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(SOToolbarRenderer))]
namespace ShoppingOi.App.Droid.Renderers
{
    public class SOToolbarRenderer : NavigationPageRenderer
    {
        public SOToolbarRenderer(Android.Content.Context context) : base(context) { }

        private Toolbar toolbar;

        public override void OnViewAdded(Android.Views.View child)
        {
            base.OnViewAdded(child);

            if (child.GetType() == typeof(Toolbar))
            {
                toolbar = (Toolbar)child;

                //int padding = Resources.GetInteger(Resource.Integer.ToolbarPadding);
                //toolbar.SetPadding(padding, padding, padding, padding);
                //toolbar.SetPadding(0, 0, 0, 0);
                toolbar.ChildViewAdded += Toolbar_ChildViewAdded;
            }
        }

        private void Toolbar_ChildViewAdded(object sender, ChildViewAddedEventArgs e)
        {
            var view = e.Child.GetType();
            if (e.Child.GetType() == typeof(AppCompatTextView))
            {
                var textView = (AppCompatTextView)e.Child;
                var spaceFont = FontManager.Instancia(MainActivity.Current.ApplicationContext.Assets).FonteBaseBold;

                textView.Typeface = spaceFont;
                textView.SetTextSize(global::Android.Util.ComplexUnitType.Dip, Resources.GetInteger(Resource.Integer.ToolbarFontSize));
                toolbar.ChildViewAdded -= Toolbar_ChildViewAdded;
            }
        }
    }
}