﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ShoppingOi.App.Droid.CustomRenderer;
using ShoppingOi.App.Droid.Util;
using ShoppingOi.App.Controls;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace ShoppingOi.App.Droid.CustomRenderer
{
    public class CustomButtonRenderer : SOButtonRenderer
    {
        public CustomButtonRenderer(Android.Content.Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            ConfiguraControle();
        }

        private void ConfiguraControle()
        {
            if (Control != null)
            {
                switch (((CustomButton)Element).Tipo)
                {
                    case CustomButton.TipoBotao.Branco:
                        Control.SetBackgroundResource(Resource.Drawable.BotaoBranco);
                        //Control.SetPadding(0, 0, 0, 0);
                        //Control.SetMinimumHeight(-1);
                        //Control.SetHeight(0);
                        //Control.SetMinHeight(0);
                        Control.SetTextColor(Resources.GetColor(Resource.Color.CinzaEscuro));
                        break;
                    default:
                        Control.SetBackgroundResource(Resource.Drawable.Botao);
                        Control.SetTextColor(Resources.GetColor(Resource.Color.CinzaClaro));
                        break;
                }
            }
        }
    }
}