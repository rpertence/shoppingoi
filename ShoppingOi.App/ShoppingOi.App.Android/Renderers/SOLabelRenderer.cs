﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using ShoppingOi.App.Droid.Renderers;
using ShoppingOi.App.Droid.Util;

[assembly: ExportRenderer(typeof(Label), typeof(SOLabelRenderer))]
namespace ShoppingOi.App.Droid.Renderers
{
    public class SOLabelRenderer : LabelRenderer
    {
        public SOLabelRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                if (e.NewElement != null && e.NewElement.FontAttributes == FontAttributes.Bold)
                    Control.Typeface = FontManager.Instancia(MainActivity.Current.Assets).FonteBaseBold;
                else
                    Control.Typeface = FontManager.Instancia(MainActivity.Current.Assets).FonteBase;
            }
        }
    }
}