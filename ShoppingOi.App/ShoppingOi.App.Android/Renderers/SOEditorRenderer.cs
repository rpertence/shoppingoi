﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using ShoppingOi.App.Droid.CustomRenderer;
using ShoppingOi.App.Droid.Util;

[assembly: ExportRenderer(typeof(Editor), typeof(SOEditorRenderer))]
namespace ShoppingOi.App.Droid.CustomRenderer
{
    public class SOEditorRenderer : EditorRenderer
    {
        public SOEditorRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetBackgroundResource(Resource.Drawable.Entry);
                Control.Typeface = FontManager.Instancia(MainActivity.Current.ApplicationContext.Assets).FonteBase;
                Control.TextAlignment = Android.Views.TextAlignment.Center;
                //Control.Gravity = GravityFlags.Center | GravityFlags.Left;
                //Control.Ellipsize = Android.Text.TextUtils.TruncateAt.Start;
                //Control.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

                var padding = Resources.GetInteger(Resource.Integer.EntryPadding);
                Control.SetPadding(padding, padding, padding, padding);
            }
        }
    }
}