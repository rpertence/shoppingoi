﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using ShoppingOi.App.Controls;
using Xamarin.Forms.Platform.Android;
using ShoppingOi.App.Droid.Renderers;
using Android.Graphics;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace ShoppingOi.App.Droid.Renderers
{
    public class CustomLabelRenderer : SOLabelRenderer
    {
        public CustomLabelRenderer(Context context) : base(context) { }

        protected CustomLabel LineSpacingLabel { get; private set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
                this.LineSpacingLabel = (CustomLabel)this.Element;

            var lineSpacing = this.LineSpacingLabel.LineHeight;

            if (LineSpacingLabel.IsUnderlined)
                Control.PaintFlags = Control.PaintFlags | PaintFlags.UnderlineText;

            this.Control.SetLineSpacing(1f, lineSpacing);

            this.UpdateLayout();
        }
    }
}