﻿using Android.Content.Res;
using Android.Graphics;

namespace ShoppingOi.App.Droid.Util
{
    public class FontManager
    {
        private AssetManager assetManager;
        private static FontManager instancia = null;

        protected FontManager()
        {
            // Exists only to defeat instantiation.
        }

        public static FontManager Instancia(AssetManager assets)
        {
            if (instancia == null)
                instancia = new FontManager { assetManager = assets };

            return instancia;
        }

        //public FontManager(AssetManager assetManager)
        //{
        //    this.assetManager = assetManager;
        //}

        private Typeface fonteBase;
        public Typeface FonteBase
        {
            get
            {
                if (fonteBase == null)
                    fonteBase = Typeface.CreateFromAsset(assetManager, "Fonts/Overlock-Regular.ttf");

                return fonteBase;
            }
        }

        private Typeface fonteBaseBold;
        public Typeface FonteBaseBold
        {
            get
            {
                if (fonteBaseBold == null)
                    fonteBaseBold = Typeface.CreateFromAsset(assetManager, "Fonts/Overlock-Bold.ttf");

                return fonteBaseBold;
            }
        }

        //private Typeface ubuntuLight;
        //public Typeface UbuntuLight
        //{
        //    get
        //    {
        //        if (ubuntuLight == null)
        //            ubuntuLight = Typeface.CreateFromAsset(assetManager, "Fonts/Ubuntu-Light.ttf");

        //        return ubuntuLight;
        //    }
        //}

        //private Typeface ubuntuMedio;
        //public Typeface UbuntuMedio
        //{
        //    get
        //    {
        //        if (ubuntuMedio == null)
        //            ubuntuMedio = Typeface.CreateFromAsset(assetManager, "Fonts/Ubuntu-Medium.ttf");

        //        return ubuntuMedio;
        //    }
        //}

        //private Typeface ubuntuBold;
        //public Typeface UbuntuBold
        //{
        //    get
        //    {
        //        if (ubuntuBold == null)
        //            ubuntuBold = Typeface.CreateFromAsset(assetManager, "Fonts/Ubuntu-Bold.ttf");

        //        return ubuntuBold;
        //    }
        //}

        //public enum TipoFonte
        //{
        //    Regular,
        //    Light,
        //    Medio,
        //    Bold
        //}
    }
}