﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using ShoppingOi.App.Util;
using ShoppingOi.App.Droid.Util;

[assembly: Dependency(typeof(Mensagem))]
namespace ShoppingOi.App.Droid.Util
{
    public class Mensagem : IMensagem
    {
        private ProgressDialog progressDialog;

        public void ShowLoading(string titulo, string mensagem)
        {
            if (progressDialog == null)
                progressDialog = new ProgressDialog(MainActivity.Current);
            else
                progressDialog.Hide();

            if (!string.IsNullOrEmpty(titulo))
                progressDialog.SetTitle(titulo);

            progressDialog.SetMessage(mensagem);

            //if (!progressDialog.IsShowing)
            //    progressDialog.Show();
            progressDialog.Show();
        }

        public void HideLoading()
        {
            if (progressDialog != null && progressDialog.IsShowing)
                progressDialog.Hide();
        }

        public void ShowShortToast(string texto)
        {
            ShowToast(texto, ToastLength.Short);
        }

        public void ShowLongToast(string texto)
        {
            ShowToast(texto, ToastLength.Long);
        }

        private void ShowToast(string texto, ToastLength length)
        {
            Toast.MakeText(MainActivity.Current, texto, length).Show();
        }

        //public void ShowConfirm(string texto)
        //{
        //    new AlertDialog.Builder(MainActivity.Current)
        //        .SetTitle("Confirmação")
        //        .SetMessage(texto)
        //        .SetOnKeyListener()
        //        .Show();
        //}
    }
}