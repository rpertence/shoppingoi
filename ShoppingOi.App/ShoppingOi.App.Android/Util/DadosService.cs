﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ShoppingOi.App.Util;
using ShoppingOi.App.Droid.Util;

[assembly: Xamarin.Forms.Dependency(typeof(DadosService))]
namespace ShoppingOi.App.Droid.Util
{
    public class DadosService : IDados
    {
        private const string PREFERENCES_NAME = "DADOS_USUARIO";

        public string GetString(string nome, string valorPadrao)
        {
            return Preferences.GetString(nome, valorPadrao);
        }

        public void SetString(string nome, string valor)
        {
            var editor = Editor;
            editor.PutString(nome, valor);
            editor.Apply();
        }

        private ISharedPreferences Preferences
        {
            get { return MainActivity.Current.GetSharedPreferences(PREFERENCES_NAME, FileCreationMode.Private); }
        }

        private ISharedPreferencesEditor Editor
        {
            get { return Preferences.Edit(); }
        }
    }
}