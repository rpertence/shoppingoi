﻿using Android.Content;
using ShoppingOi.App.Droid.Util;
using ShoppingOi.Service.Util;

[assembly: Xamarin.Forms.Dependency(typeof(Conexao))]
namespace ShoppingOi.App.Droid.Util
{
    public class Conexao : IConexao
    {
        private const string BASE_URL_API = "http://192.168.15.2/ShoppingOi.WebApi/api{0}";

        public bool EstaConectado()
        {
            Android.Net.ConnectivityManager connectivityManager = (Android.Net.ConnectivityManager)MainActivity.Current.GetSystemService(Context.ConnectivityService);
            Android.Net.NetworkInfo activeNetworkInfo = connectivityManager.ActiveNetworkInfo;

            return activeNetworkInfo != null && activeNetworkInfo.IsConnected;
        }

        public string GetBaseUrlApi()
        {
            return BASE_URL_API;
        }
    }
}