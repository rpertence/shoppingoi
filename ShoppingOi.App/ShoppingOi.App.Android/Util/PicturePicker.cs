﻿using System.IO;
using System.Threading.Tasks;
using Android.Content;
using ShoppingOi.App.Droid.Util;
using ShoppingOi.App.Util;
using Xamarin.Forms;

[assembly: Dependency(typeof(PicturePicker))]
namespace ShoppingOi.App.Droid.Util {
    public class PicturePicker : IPicturePicker {
        public Task<Stream> GetImageStreamAsync() {
            // Define the Intent for getting images
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionPick);

            // Start the picture-picker activity (resumes in MainActivity.cs)
            MainActivity.Current.StartActivityForResult(
                Intent.CreateChooser(intent, "Selecione a imagem"),
                MainActivity.PickImageId);

            // Save the TaskCompletionSource object as a MainActivity property
            MainActivity.Current.PickImageTaskCompletionSource = new TaskCompletionSource<Stream>();

            // Return Task object
            return MainActivity.Current.PickImageTaskCompletionSource.Task;
        }
    }
}