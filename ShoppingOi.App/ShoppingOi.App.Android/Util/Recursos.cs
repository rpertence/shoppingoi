﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using ShoppingOi.App.Util;
using Android.Graphics;
using Android;
using Android.Content.Res;
using ShoppingOi.App.Droid.Util;

[assembly: Xamarin.Forms.Dependency(typeof(Recursos))]
namespace ShoppingOi.App.Droid.Util
{
    public class Recursos : IRecursos
    {
        public float GetDimension(Dimensions dim)
        {
            switch (dim)
            {
                //case Dimensions.FontePequena:
                //    return Forms.Context.Resources.GetDimension(Resource.Dimension.FontePequena);
                //case Dimensions.FonteMedia:
                //    return Forms.Context.Resources.GetDimension(Resource.Dimension.FonteMedia);
                //case Dimensions.FonteGrande:
                //    return Forms.Context.Resources.GetDimension(Resource.Dimension.FonteGrande);
                default:
                    return 0;
            }
        }
    }
}