﻿using Realms;
using ShoppingOi.App.Util;

namespace ShoppingOi.App.Model
{
    public class Usuario : RealmObject
    {
        [PrimaryKey]
        public string Codigo { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string CodigoLoja { get; set; }

        public void Salvar(string codigoLoja)
        {
            var realm = this.Realm ?? RealmManager.GetInstance();

            realm.Write(() =>
            {
                CodigoLoja = codigoLoja;
                realm.Add(this, true);
            });
        }
    }
}
