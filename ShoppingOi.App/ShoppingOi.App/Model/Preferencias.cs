﻿using Realms;
using ShoppingOi.App.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingOi.App.Model
{
    public class Preferencias : RealmObject
    {
        public int CodigoPerfilAtivo { get; set; }

        public Perfil PerfilAtivo
        {
            get { return (Perfil)CodigoPerfilAtivo; }
        }

        public enum Perfil
        {
            Cliente,
            Lojista
        }

        public static Preferencias Item()
        {
            var realm = RealmManager.GetInstance();
            return realm.All<Preferencias>().FirstOrDefault();
        }

        public void Salvar(Perfil perfilAtivo)
        {
            var realm = this.Realm ?? RealmManager.GetInstance();

            realm.Write(() => 
            {
                this.CodigoPerfilAtivo = (int)perfilAtivo;
                realm.Add(this, true);
            });
        }
    }
}
