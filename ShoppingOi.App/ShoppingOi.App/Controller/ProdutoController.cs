﻿using ShoppingOi.App.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingOi.App.Controller
{
    public class ProdutoController
    {
        public ProdutoCadastroViewModel GetCadastroViewModel() {
            return new ProdutoCadastroViewModel {
                Titulo = "Novo Produto"
            };
        }
    }
}
