﻿namespace ShoppingOi.App.Util
{
    public interface IDados
    {
        void SetString(string nome, string valor);

        string GetString(string nome, string valorPadrao);
    }
}
