﻿namespace ShoppingOi.App.Util
{
    public interface IRecursos
    {
        float GetDimension(Dimensions dim);
    }

    public enum Dimensions
    {
        FontePequena,
        FonteMedia,
        FonteGrande
    }
}
