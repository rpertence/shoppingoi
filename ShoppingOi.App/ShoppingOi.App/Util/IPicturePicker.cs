﻿using System.IO;
using System.Threading.Tasks;

namespace ShoppingOi.App.Util {
    public interface IPicturePicker {
        Task<Stream> GetImageStreamAsync();
    }
}
