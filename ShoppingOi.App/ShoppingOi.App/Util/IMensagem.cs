﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingOi.App.Util
{
    public interface IMensagem
    {
        void ShowLoading(string titulo, string mensagem);

        void HideLoading();

        void ShowShortToast(string texto);

        void ShowLongToast(string texto);

        //void ShowConfirm(string texto);
    }
}
