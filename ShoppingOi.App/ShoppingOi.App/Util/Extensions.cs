﻿using ShoppingOi.App.Model;
using ShoppingOi.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingOi.App.Util
{
    public static class Extensions
    {
        public static Usuario ToModel(this UsuarioApp app)
        {
            return new Usuario
            {
                Codigo = app.Codigo,
                Nome = app.Nome,
                Email = app.Email,
                CodigoLoja = app.CodigoLoja
            };
        }
    }
}
