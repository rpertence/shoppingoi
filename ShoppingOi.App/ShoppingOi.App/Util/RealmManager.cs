﻿using Realms;
using ShoppingOi.App.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingOi.App.Util
{
    public class RealmManager
    {
        public static Realm GetInstance()
        {
            var config = new RealmConfiguration() { SchemaVersion = 2 };
            return Realm.GetInstance(config);
        }

        public static void Sair()
        {
            var realm = GetInstance();
            realm.Write(() =>
            {
                realm.RemoveAll<Usuario>();
            });
        }
    }
}
