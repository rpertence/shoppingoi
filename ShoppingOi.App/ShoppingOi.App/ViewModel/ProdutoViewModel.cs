﻿using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;

namespace ShoppingOi.App.ViewModel {
    public class ProdutoCadastroViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        public string Titulo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string PalavrasChave { get; set; }
        public List<Imagem> Imagens { get; set; }

        //private IEnumerable<Imagem> imagem;
        //public IEnumerable<Imagem> Imagens {
        //    get { return imagem; }
        //    set { imagem = value; }
        //}
        public void AddImagem(Imagem imagem) {
            if (Imagens == null)
                Imagens = new List<Imagem>();

            Imagens.Add(imagem);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Imagens"));
        }

        public class Imagem {
            public ImageSource Source { get; set; }
        }
    }
}
