﻿using ShoppingOi.DTO;
using ShoppingOi.Service;
using ShoppingOi.Service.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;

namespace ShoppingOi.App.ViewModel
{
    public class LojasViewModel
    {
        public List<GrupoLojas> Lojas { get; set; }

        public LojasViewModel GetViewModel(IEnumerable<LojaApp> lojas)
        {
            Lojas = new List<GrupoLojas>();

            var grupos = lojas.GroupBy(l => l.Categoria);

            foreach (var item in grupos.OrderBy(g => g.Key))
                Lojas.Add(new GrupoLojas(item.Key.ToUpper(), item.OrderBy(l => l.Nome)));

            return this;
        }

        public class GrupoLojas : ObservableCollection<LojaApp>
        {
            public string Categoria { get; set; }

            public GrupoLojas(string categoria, IEnumerable<LojaApp> lojas)
            {
                Categoria = categoria;

                foreach (var loja in lojas)
                    this.Items.Add(loja);
            }
        }
    }

    public class LojaViewModel
    {
        public LojaApp Loja { get; set; }

        public LojaViewModel(LojaApp loja)
        {
            Loja = loja;
            Loja.Nome = Loja.Nome.ToUpper();
        }
    }
}