﻿using ShoppingOi.DTO;
using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroLoja : BasePage
    {
        private LojaApp loja;

        public CadastroLoja()
        {
            InitializeComponent();

            entryNome.TextChanged += (sender, e) =>
            {
                if (entryNome.Text.Length > 50)
                    entryNome.Text = entryNome.Text.Substring(0, 50);

                labelCaracteresNome.Text = $"{entryNome.Text.Length} de 50 caracteres";
            };

            editorDescricao.TextChanged += (sender, e) =>
            {
                if (editorDescricao.Text.Length > 500)
                    editorDescricao.Text = editorDescricao.Text.Substring(0, 500);

                labelCaracteresDescricao.Text = $"{editorDescricao.Text.Length} de 500 caracteres";
            };

            editorLocalizacao.TextChanged += (sender, e) =>
            {
                if (editorLocalizacao.Text.Length > 100)
                    editorLocalizacao.Text = editorLocalizacao.Text.Substring(0, 100);

                labelCaracteresLocalizacao.Text = $"{editorLocalizacao.Text.Length} de 100 caracteres";
            };

            btnAvancar.Clicked += async (sender, e) =>
            {
                var nome = entryNome.Text;
                var descricao = editorDescricao.Text;
                var localizacao = editorLocalizacao.Text;

                if (string.IsNullOrEmpty(nome) || string.IsNullOrEmpty(descricao) || string.IsNullOrEmpty(localizacao))
                    ExibeMensagemErro("Todos os campos são obrigatórios!");
                else
                {
                    loja.Nome = nome;
                    loja.Descricao = descricao;
                    loja.Localizacao = localizacao;

                    await Navigation.PushAsync(new CadastroLojaCategorias(loja));
                }
            };

            CarregaLoja();
        }

        private async void CarregaLoja()
        {
            if (Usuario != null && !string.IsNullOrEmpty(Usuario.CodigoLoja))
            {
                ShowLoading();

                var retorno = await new LojaService(ConexaoService).BuscaItem(Usuario.CodigoLoja);

                if (!retorno.Sucesso)
                    TrataErroRetorno(retorno);
                else
                {
                    loja = (LojaApp)retorno;

                    entryNome.Text = loja.Nome;
                    editorDescricao.Text = loja.Descricao;
                    editorLocalizacao.Text = loja.Localizacao;
                }

                HideLoading();
            }
            else
                loja = new LojaApp();
        }
    }
}