﻿using ShoppingOi.Service;
using ShoppingOi.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.ComponentModel;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroLojaCategorias : BasePage
    {
        private LojaApp loja;

        public CadastroLojaCategorias(LojaApp loja)
        {
            InitializeComponent();

            this.loja = loja;

            CarregaCategorias();

            btnSalvar.Clicked += BtnSalvar_Clicked;
        }

        private async void BtnSalvar_Clicked(object sender, EventArgs e)
        {
            var categorias = listaCategorias.ItemsSource as List<CategoriaItem>;

            if (categorias.Count(c => c.Selecionada) == 0)
                ExibeMensagemErro("Selecione ao menos uma categoria!");
            else
            {
                loja.CodigoUsuario = Usuario.Codigo;
                loja.Categorias = categorias.Where(c => c.Selecionada).Select(c => new CategoriaApp
                {
                    Codigo = c.Codigo
                }).ToList();

                ShowLoading();

                var retorno = await new LojaService(ConexaoService).Salvar(loja);

                if (!retorno.Sucesso)
                    TrataErroRetorno(retorno);
                else
                {
                    Usuario.Salvar(((LojaApp)retorno).Codigo);
                    await PopAllTo(new HomeLojista());
                }

                HideLoading();
            }
        }

        private async void CarregaCategorias()
        {
            ShowLoading();

            var retorno = await new CategoriaService(ConexaoService).BuscaLista();

            if (!retorno.Sucesso)
                TrataErroRetorno(retorno);
            else
            {
                var categorias = ((CategoriaAppCollection)retorno).Categorias.OrderBy(c => c.Descricao);
                var lista = new List<CategoriaItem>();

                foreach (var item in categorias)
                {
                    var ci = new CategoriaItem();
                    ci.Codigo = item.Codigo;
                    ci.Descricao = item.Descricao;
                    ci.Selecionada = loja.LojasCategorias == null ? false : loja.LojasCategorias.Any(lc => lc.CodigoCategoria.Equals(item.Codigo));
                    lista.Add(ci);
                }
                //.Select(c =>
                //new CategoriaItem
                //{
                //    Codigo = c.Codigo,
                //    Descricao = c.Descricao,
                //    Selecionada = loja.LojasCategorias.Any(lc => lc.CodigoCategoria.Equals(c.Codigo))
                //}).ToList();

                listaCategorias.ItemsSource = lista;
            }

            HideLoading();
        }

        private void listaCategorias_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (listaCategorias.SelectedItem != null)
            {
                var item = (CategoriaItem)listaCategorias.SelectedItem;
                item.Selecionada = !item.Selecionada;
                listaCategorias.SelectedItem = null;
            }
        }

        public class CategoriaItem : INotifyPropertyChanged
        {
            public string Codigo { get; set; }

            public string Descricao { get; set; }

            private bool selecionada;
            public bool Selecionada
            {
                get { return selecionada; }
                set
                {
                    if (!value.Equals(selecionada))
                    {
                        selecionada = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Selecionada"));
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }
    }
}