﻿using ShoppingOi.App.Model;
using ShoppingOi.App.Util;
using ShoppingOi.DTO;
using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : BasePage
    {
        private Destino destino;
        public Login(Destino destino)
        {
            InitializeComponent();

            this.destino = destino;

            TapGestureRecognizer tapEsqueciSenha = new TapGestureRecognizer();
            tapEsqueciSenha.Tapped += async (sender, e) => { await Navigation.PushAsync(new EsqueciSenha(destino)); };
            lblEsqueciSenha.GestureRecognizers.Add(tapEsqueciSenha);

            TapGestureRecognizer tapCriarConta = new TapGestureRecognizer();
            tapCriarConta.Tapped += async (sender, e) => { await Navigation.PushAsync(new CriarConta("", destino)); };
            lblCriarConta.GestureRecognizers.Add(tapCriarConta);

            btnEntrar.Clicked += Login_Clicked;
        }

        private async void Login_Clicked(object sender, EventArgs e)
        {
            var email = entryEmail.Text;
            var senha = entrySenha.Text;

            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(senha))
            {
                ExibeMensagemErro("Informe o e-mail e a senha!");
                return;
            }

            ShowLoading();

            var retorno = await new UsuarioService(ConexaoService).Login(email, senha);

            if (!retorno.Sucesso)
            {
                if (retorno.CodigoRetorno == BaseApiClass.CodigoRetornoEnum.CadastroNaoConfirmado)
                    await Navigation.PushAsync(new ConfirmarCodigo(destino, null, email, "Informe o código enviado para o seu e-mail para confirmar o cadastro:"));
                else
                    TrataErroRetorno(retorno);
            }
            else
            {
                Usuario = ((UsuarioApp)retorno).ToModel();

                await RedirectToDestino(destino);
                //switch (destino)
                //{
                //    case Destino.HomeCliente:
                //        await PopAllTo(new HomeCliente());
                //        break;
                //    case Destino.HomeLojista:
                //        await PopAllTo(new HomeLojista());
                //        break;
                //    case Destino.CadastroLoja:
                //        await PopAllTo(new CadastroLoja());
                //        break;
                //    default:
                //        await PopAllTo(new MainPage());
                //        break;
                //}
            }

            HideLoading();
        }
    }
}