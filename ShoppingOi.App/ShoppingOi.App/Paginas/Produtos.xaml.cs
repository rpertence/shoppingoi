﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Produtos : ContentPage {
        public Produtos() {
            InitializeComponent();
        }

        private async void NovoProduto_Clicked(object sender, EventArgs e) {
            await Navigation.PushAsync(new CadastroProduto());
        }
    }
}