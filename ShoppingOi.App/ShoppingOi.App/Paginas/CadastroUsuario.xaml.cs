﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ShoppingOi.DTO;
using ShoppingOi.Service;
using ShoppingOi.App.Util;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroUsuario : BasePage
    {
        private Destino destino;

        public CadastroUsuario(Destino destino)
        {
            InitializeComponent();

            this.destino = destino;

            entryNome.TextChanged += (sender, e) =>
            {
                if (entryNome.Text.Length > 50)
                    entryNome.Text = entryNome.Text.Substring(0, 50);

                labelCaracteresNome.Text = $"{entryNome.Text.Length} de 50 caracteres";
            };

            entryMail.TextChanged += (sender, e) =>
            {
                if (entryMail.Text.Length > 50)
                    entryMail.Text = entryMail.Text.Substring(0, 50);

                labelCaracteresEmail.Text = $"{entryMail.Text.Length} de 50 caracteres";
            };

            entrySenha.TextChanged += (sender, e) =>
            {
                if (entrySenha.Text.Length > 12)
                    entrySenha.Text = entrySenha.Text.Substring(0, 12);
            };

            entryConfirmacaoSenha.TextChanged += (sender, e) =>
            {
                if (entryConfirmacaoSenha.Text.Length > 12)
                    entryConfirmacaoSenha.Text = entryConfirmacaoSenha.Text.Substring(0, 12);
            };

            btnSalvar.Clicked += async (sender, e) =>
            {
                var nome = entryNome.Text;
                var email = entryMail.Text;
                var senha = entrySenha.Text;
                var senha2 = entryConfirmacaoSenha.Text;

                Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
                if (string.IsNullOrEmpty(nome) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(senha) || string.IsNullOrEmpty(senha2))
                    ExibeMensagemErro("Todos os campos são obrigatórios");
                else if (!rg.IsMatch(email))
                    ExibeMensagemErro("Informe um endereço de e-mail válido");
                else if (!senha.Equals(senha2))
                    ExibeMensagemErro("As senhas digitadas não conferem");
                else if (senha.Length < 6 || senha.Length > 12)
                    ExibeMensagemErro("A senha deve ter entre 6 e 12 caracteres");
                else
                {
                    UsuarioApp usuario = new UsuarioApp
                    {
                        Nome = nome,
                        Email = email,
                        Senha = senha
                    };

                    ShowLoading();

                    var retorno = await new UsuarioService(ConexaoService).Criar(usuario);

                    if (!retorno.Sucesso)
                        TrataErroRetorno(retorno);
                    else
                    {
                        Usuario = ((UsuarioApp)retorno).ToModel();
                        await PopAllTo(new ConfirmarCodigo(destino, null, email, "Informe o código enviado para o seu e-mail para confirmar o cadastro:"));
                    }

                    HideLoading();
                }
            };
        }
    }
}