﻿using ShoppingOi.App.ViewModel;
using ShoppingOi.DTO;
using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Loja : BasePage
    {
        public Loja(string codigo)
        {
            InitializeComponent();

            CarregaLoja(codigo);
        }

        private async void CarregaLoja(string codigo)
        {
            ShowLoading();

            var retorno = await new LojaService(ConexaoService).BuscaItem(codigo);

            if (!retorno.Sucesso)
                TrataErroRetorno(retorno);
            else
                BindingContext = new LojaViewModel((LojaApp)retorno);

            HideLoading();
        }
    }
}