﻿using ShoppingOi.DTO;
using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Categorias : BasePage
    {
        public Categorias()
        {
            InitializeComponent();

            CarregaCategorias();
        }

        private async void CarregaCategorias()
        {
            ShowLoading();

            var retorno = await new CategoriaService(ConexaoService).BuscaLista();

            if (!retorno.Sucesso)
                TrataErroRetorno(retorno);
            else
                listaCategorias.ItemsSource = ((CategoriaAppCollection)retorno).Categorias.OrderBy(c => c.Descricao);

            HideLoading();
        }
    }
}