﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ShoppingOi.App.Controller;
using ShoppingOi.App.ViewModel;

namespace ShoppingOi.App.Paginas {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroProduto : ContentPage {
        private ProdutoCadastroViewModel viewModel;
        public CadastroProduto() {
            InitializeComponent();

            viewModel = new ProdutoController().GetCadastroViewModel();
            BindingContext = viewModel;
        }

        protected async void Avancar_Click(object sender, EventArgs e) {
            await Navigation.PushAsync(new CadastroProdutoImagens(viewModel));
        }
    }
}