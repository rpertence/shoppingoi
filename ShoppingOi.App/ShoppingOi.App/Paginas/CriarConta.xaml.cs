﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CriarConta : BasePage
    {
        public CriarConta(string mensagem, Destino destino)
        {
            InitializeComponent();

            Mensagem = mensagem;
            this.BindingContext = this;

            var tapBlocoCadastro = new TapGestureRecognizer();
            tapBlocoCadastro.Tapped += async (sender, e) => { await Navigation.PushAsync(new CadastroUsuario(destino)); };
            blocoCadastro.GestureRecognizers.Add(tapBlocoCadastro);

            var tapLogin = new TapGestureRecognizer();
            tapLogin.Tapped += async (sender, e) => { await Navigation.PushAsync(new Login(Paginas.Login.Destino.HomeCliente)); };
            lblJaTenhoConta.GestureRecognizers.Add(tapLogin);
        }

        public string Mensagem { get; set; }
    }
}