﻿using ShoppingOi.App.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeCliente : BasePage
    {
        public HomeCliente()
        {
            InitializeComponent();

            CarregaDadosUsuario();

            TapGestureRecognizer tapCategorias = new TapGestureRecognizer();
            tapCategorias.Tapped += async (sender, e) => { await Navigation.PushAsync(new Categorias()); };
            frameCategorias.GestureRecognizers.Add(tapCategorias);

            TapGestureRecognizer tapTrocarPerfil = new TapGestureRecognizer();
            tapTrocarPerfil.Tapped += async (sender, e) => { await PopAllTo(new MainPage()); };
            frameTrocarPerfil.GestureRecognizers.Add(tapTrocarPerfil);

            TapGestureRecognizer tapLojas = new TapGestureRecognizer();
            tapLojas.Tapped += async (sender, e) => { await Navigation.PushAsync(new Lojas()); };
            frameLojas.GestureRecognizers.Add(tapLojas);

            TapGestureRecognizer tapSair = new TapGestureRecognizer();
            tapSair.Tapped += async (sender, e) =>
            {
                var resultado = await DisplayAlert("Confirmação", "Tem certeza que deseja sair?", "Sair", "Cancelar");

                if (resultado)
                {
                    RealmManager.Sair();
                    CarregaDadosUsuario();
                }
            };
            frameSair.GestureRecognizers.Add(tapSair);
        }

        private void CarregaDadosUsuario()
        {
            if (Usuario != null)
            {
                lblUsuario.Text = $"Bem-vindo(a), {Usuario.Nome}";
                frameSair.IsVisible = true;
            }
            else
            {
                lblUsuario.Text = "Você não está logado.";

                btnLogin.IsVisible = true;
                btnLogin.Clicked += async (sender, e) => { await Navigation.PushAsync(new Login(Destino.HomeCliente)); };

                //TapGestureRecognizer tapLogin = new TapGestureRecognizer();
                //tapLogin.Tapped += async (sender, e) => { await Navigation.PushAsync(new Login(Destino.HomeCliente)); };
                //lblLogin.GestureRecognizers.Add(tapLogin);

                frameSair.IsVisible = false;
            }
        }

        //private async void Sair(object sender, EventArgs e)
        //{
        //    Usuario
        //}
    }
}