﻿using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EsqueciSenha : BasePage
    {
        private Destino destino;

        public EsqueciSenha(Destino destino)
        {
            InitializeComponent();

            this.destino = destino;

            btnEnviar.Clicked += BtnEnviar_Clicked;
        }

        private async void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            var email = entryEmail.Text;

            if (string.IsNullOrEmpty(email))
                ExibeMensagemErro("Informe o e-mail de cadastro!");
            else
            {
                ShowLoading();

                var retorno = await new UsuarioService(ConexaoService).EnviarCodigoRecuperacaoSenha(email);

                if (!retorno.Sucesso)
                    TrataErroRetorno(retorno);
                else
                    await Navigation.PushAsync(new ConfirmarCodigo(Destino.AlterarSenha, destino, email, "Informe o código enviado para o seu e-mail para alterar a senha:"));

                HideLoading();
            }
        }
    }
}