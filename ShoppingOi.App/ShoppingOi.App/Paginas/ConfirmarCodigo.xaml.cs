﻿using ShoppingOi.DTO;
using ShoppingOi.Service;
using ShoppingOi.App.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmarCodigo : BasePage
    {
        private Destino destino;
        private Destino? destino2;
        private string email;

        public ConfirmarCodigo(Destino destino, Destino? destino2, string email, string mensagem)
        {
            InitializeComponent();

            this.destino = destino;
            this.destino2 = destino2;
            this.email = email;
            lblMensagem.Text = mensagem;

            btnEnviar.Clicked += BtnEnviar_Clicked;

            lblReenviar.Text = $"Reenviar código para {email}";
            var tapReenviar = new TapGestureRecognizer();
            tapReenviar.Tapped += ReenviarCodigo;
            lblReenviar.GestureRecognizers.Add(tapReenviar);
        }

        private async void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            var codigo = entryCodigo.Text;

            if (string.IsNullOrEmpty(codigo))
                ExibeMensagemErro("Informe o código!");
            else
            {
                ShowLoading();

                var retorno = await new UsuarioService(ConexaoService).ConfirmarCodigo(email, codigo);

                if (!retorno.Sucesso)
                    TrataErroRetorno(retorno);
                else
                {
                    Usuario = ((UsuarioApp)retorno).ToModel();
                    await RedirectToDestino(destino, destino2, email);
                }

                HideLoading();
            }
        }

        private async void ReenviarCodigo(object sender, EventArgs e)
        {
            ShowLoading();

            var retorno = await new UsuarioService(ConexaoService).ReenviarCodigo(email, destino == Destino.AlterarSenha ? 2 : 1);

            if (!retorno.Sucesso)
                TrataErroRetorno(retorno);
            else
                ExibeMensagem("Sucesso!", "O código de confirmação foi enviado para o seu e-mail.");

            HideLoading();
        }
    }
}