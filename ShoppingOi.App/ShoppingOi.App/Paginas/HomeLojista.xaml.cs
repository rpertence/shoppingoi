﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeLojista : BasePage {
        public HomeLojista() {
            InitializeComponent();

            if (Usuario != null)
                lblUsuario.Text = $"Bem-vindo(a), {Usuario.Nome}";
            else {
                lblUsuario.Text = "Você não está logado.";

                lblLogin.IsVisible = true;

                TapGestureRecognizer tapLogin = new TapGestureRecognizer();
                tapLogin.Tapped += async (sender, e) => { await Navigation.PushAsync(new Login(Destino.HomeLojista)); };
                lblLogin.GestureRecognizers.Add(tapLogin);
            }

            TapGestureRecognizer tapTrocarPerfil = new TapGestureRecognizer();
            tapTrocarPerfil.Tapped += async (sender, e) => { await PopAllTo(new MainPage()); };
            frameTrocarPerfil.GestureRecognizers.Add(tapTrocarPerfil);

            TapGestureRecognizer tapMinhaLoja = new TapGestureRecognizer();
            tapMinhaLoja.Tapped += async (sender, e) => {
                if (Usuario == null) {
                    await Navigation.PushAsync(new Login(Destino.HomeLojista));
                    ExibeMensagem("Aviso", "Você precisa fazer login para cadastrar sua loja");
                }
                else
                    await Navigation.PushAsync(new CadastroLoja());
            };
            frameMinhaLoja.GestureRecognizers.Add(tapMinhaLoja);
        }

        private async void FrameProdutos_Tapped(object sender, EventArgs e) {
            await Navigation.PushAsync(new Produtos());
        }
    }
}