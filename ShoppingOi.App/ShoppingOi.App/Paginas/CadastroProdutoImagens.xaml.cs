﻿using ShoppingOi.App.Util;
using ShoppingOi.App.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroProdutoImagens : ContentPage {
        private ProdutoCadastroViewModel viewModel;
        public CadastroProdutoImagens(ProdutoCadastroViewModel viewModel) {
            InitializeComponent();

            this.viewModel = viewModel;
            BindingContext = viewModel;
        }

        private async void Button_Clicked(object sender, EventArgs e) {
            var btn = sender as Button;
            btn.IsEnabled = false;

            Stream stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();

            if (stream != null) {
                viewModel.AddImagem(new ProdutoCadastroViewModel.Imagem { Source = ImageSource.FromStream(() => stream) });
                //Image image = new Image {
                //    Source = ,
                //    BackgroundColor = Color.Gray
                //};

                //TapGestureRecognizer recognizer = new TapGestureRecognizer();
                //recognizer.Tapped += (sender2, args) => {
                //    //(MainPage as ContentPage).Content = stack;
                //    btn.IsEnabled = true;
                //};
                //image.GestureRecognizers.Add(recognizer);

                //((StackLayout)this.Content).Children.Add(image);
                btn.IsEnabled = true;
                //(MainPage as ContentPage).Content = image;
            }
            else {
                btn.IsEnabled = true;
            }
        }

        private void Salvar_Clicked(object sender, EventArgs e) {

        }
    }
}