﻿using ShoppingOi.App.ViewModel;
using ShoppingOi.DTO;
using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Lojas : BasePage
    {
        public Lojas()
        {
            InitializeComponent();

            CarregaLojas();
        }

        private async void CarregaLojas()
        {
            ShowLoading();

            var retorno = await new LojaService(ConexaoService).BuscaLista();

            if (!retorno.Sucesso)
                TrataErroRetorno(retorno);
            else
                BindingContext = new LojasViewModel().GetViewModel(((LojaAppCollection)retorno).Lojas);

            HideLoading();
        }

        private async void ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (lvLojas.SelectedItem != null)
            {
                var lojaApp = lvLojas.SelectedItem as LojaApp;
                lvLojas.SelectedItem = null;

                await Navigation.PushAsync(new Loja(lojaApp.Codigo));
            }
        }
    }
}