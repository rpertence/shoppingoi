﻿using ShoppingOi.DTO;
using ShoppingOi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingOi.App.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlterarSenha : BasePage
    {
        private Destino destino;
        private string email;

        public AlterarSenha(Destino destino, string email)
        {
            InitializeComponent();

            this.destino = destino;
            this.email = email;

            entrySenha.TextChanged += (sender, e) =>
            {
                if (entrySenha.Text.Length > 12)
                    entrySenha.Text = entrySenha.Text.Substring(0, 12);
            };

            entryConfirmacaoSenha.TextChanged += (sender, e) =>
            {
                if (entryConfirmacaoSenha.Text.Length > 12)
                    entryConfirmacaoSenha.Text = entryConfirmacaoSenha.Text.Substring(0, 12);
            };

            btnSalvar.Clicked += async (sender, e) =>
            {
                var senha = entrySenha.Text;
                var senha2 = entryConfirmacaoSenha.Text;

                if (!senha.Equals(senha2))
                    ExibeMensagemErro("As senhas digitadas não conferem");
                else if (senha.Length < 6 || senha.Length > 12)
                    ExibeMensagemErro("A senha deve ter entre 6 e 12 caracteres");
                else
                {
                    var usuario = new UsuarioApp
                    {
                        Email = email,
                        Senha = senha
                    };

                    ShowLoading();

                    var retorno = await new UsuarioService(ConexaoService).AlterarSenha(usuario);

                    if (!retorno.Sucesso)
                        TrataErroRetorno(retorno);
                    else
                        await RedirectToDestino(destino);

                    HideLoading();
                }
            };
        }
    }
}