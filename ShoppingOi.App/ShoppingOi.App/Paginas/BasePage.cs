﻿using Realms;
using ShoppingOi.App.Model;
using ShoppingOi.App.Util;
using ShoppingOi.DTO;
using ShoppingOi.Service.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ShoppingOi.App.Paginas
{
    public class BasePage : ContentPage
    {
        public async Task PopAllTo(Page page)
        {
            if (page == null) return;

            Navigation.InsertPageBefore(page, Navigation.NavigationStack.First());

            await Navigation.PopToRootAsync();
        }

        protected void ExibeMensagemErro(string mensagem)
        {
            ExibeMensagem("Erro", mensagem);
        }

        protected void ExibeMensagem(string titulo, string mensagem)
        {
            ExibeMensagem(titulo, mensagem, "OK");
        }

        protected void ExibeMensagem(string titulo, string mensagem, string textoOk)
        {
            DisplayAlert(titulo, mensagem, textoOk);
        }

        public void ShowLoading()
        {
            ShowLoading(null, "Aguarde!");
        }

        protected void ShowLoading(string titulo, string mensagem)
        {
            MensagemService.ShowLoading(titulo, mensagem);
        }

        public void HideLoading()
        {
            MensagemService.HideLoading();
        }

        public void TrataErroRetorno(BaseApiClass retorno)
        {
            HideLoading();

            switch (retorno.CodigoRetorno)
            {
                //case BaseApiClass.CodigoRetornoEnum.DadosNaoEncontrados:
                //    ExibeMensagemErro(Constantes.ERRO_DADOS_NAO_ENCONTRADOS);
                //    break;
                //case BaseApiClass.CodigoRetornoEnum.EmailJaCadastrado:
                //    ExibeMensagemErro(Constantes.ERRO_EMAIL_JA_CADASTRADO);
                //    break;
                //case BaseApiClass.CodigoRetornoEnum.TelefoneNaoConectado:
                //    ExibeMensagemErro(Constantes.ERRO_TELEFONE_NAO_CONECTADO);
                //    break;
                case BaseApiClass.CodigoRetornoEnum.MetodoNaoEncontrado:
                    ExibeMensagemErro("Método não encontrado!");
                    break;
                default:
                    ExibeMensagemErro(retorno.Mensagem);
                    break;
            }
        }

        public async Task RedirectToDestino(Destino destino)
        {
            await RedirectToDestino(destino, null, null);
        }

        public async Task RedirectToDestino(Destino destino, Destino? destino2, object parametro)
        {
            switch (destino)
            {
                case Destino.HomeCliente:
                    await PopAllTo(new HomeCliente());
                    break;
                case Destino.HomeLojista:
                    await PopAllTo(new HomeLojista());
                    break;
                case Destino.CadastroLoja:
                    await PopAllTo(new CadastroLoja());
                    break;
                case Destino.AlterarSenha:
                    await Navigation.PushAsync(new AlterarSenha(destino2.Value, parametro.ToString()));
                    break;
                default:
                    await PopAllTo(new MainPage());
                    break;
            }
        }

        private IMensagem mensagemService;
        private IMensagem MensagemService
        {
            get
            {
                if (mensagemService == null)
                    mensagemService = DependencyService.Get<IMensagem>();

                return mensagemService;
            }
        }

        private IConexao conexaoService;
        public IConexao ConexaoService
        {
            get
            {
                if (conexaoService == null)
                    conexaoService = DependencyService.Get<IConexao>();

                return conexaoService;
            }
        }

        public Usuario Usuario
        {
            get
            {
                var realm = RealmManager.GetInstance();
                return realm.All<Usuario>().FirstOrDefault();
            }
            set
            {
                var realm = RealmManager.GetInstance();
                realm.Write(() =>
                {
                    realm.RemoveAll<Usuario>();
                    realm.Add(value);
                });
            }
        }

        public enum Destino
        {
            HomeCliente,
            HomeLojista,
            CadastroLoja,
            AlterarSenha
        }
    }
}
