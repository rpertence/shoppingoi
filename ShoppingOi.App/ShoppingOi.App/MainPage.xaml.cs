﻿using ShoppingOi.App.Model;
using ShoppingOi.App.Paginas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ShoppingOi.App
{
    public partial class MainPage : BasePage
    {
        private bool? clienteEscolhido;
        public MainPage()
        {
            InitializeComponent();

            var tapCliente = new TapGestureRecognizer();
            tapCliente.Tapped += (sender, e) =>
            {
                clienteEscolhido = true;
                checkCliente.IsVisible = true;
                checkLojista.IsVisible = false;
            };

            frameCliente.GestureRecognizers.Add(tapCliente);

            var tapLojista = new TapGestureRecognizer();
            tapLojista.Tapped += (sender, e) =>
            {
                clienteEscolhido = false;
                checkCliente.IsVisible = false;
                checkLojista.IsVisible = true;
            };

            framLojista.GestureRecognizers.Add(tapLojista);

            btnAvancar.Clicked += async (sender, e) =>
            {
                if (!clienteEscolhido.HasValue)
                    ExibeMensagemErro("Escolha o seu perfil");
                else
                {
                    var preferencias = Preferencias.Item();

                    if (preferencias == null)
                        preferencias = new Preferencias();

                    if (clienteEscolhido.Value)
                    {
                        preferencias.Salvar(Preferencias.Perfil.Cliente);
                        await PopAllTo(new HomeCliente());
                    }
                    else
                    {
                        preferencias.Salvar(Preferencias.Perfil.Lojista);
                        await PopAllTo(new HomeLojista());

                        //if (Usuario == null)
                        //    await PopAllTo(new CriarConta("Você precisa estar cadastrado para usar o perfil lojista."));
                        //else
                        //    await PopAllTo(new CadastroLoja());
                    }
                }
            };
        }
    }
}
