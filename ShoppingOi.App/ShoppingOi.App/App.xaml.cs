﻿using ShoppingOi.App.Model;
using ShoppingOi.App.Paginas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Realms;

namespace ShoppingOi.App
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //#if __ANDROID__
            //            Current.Resources["FontePequena"] = (double)Forms.Context.Resources.GetDimension(Droid.Resource.Dimension.FontePequena);
            //            Current.Resources["FonteMedia"] = (double)Forms.Context.Resources.GetDimension(Droid.Resource.Dimension.FonteMedia);
            //            Current.Resources["FonteGrande"] = (double)Forms.Context.Resources.GetDimension(Droid.Resource.Dimension.FonteGrande);
            //#endif
            var preferencias = Preferencias.Item();

            Page root;

            if (preferencias == null)
                root = new MainPage();
            else
            {
                if (preferencias.PerfilAtivo == Preferencias.Perfil.Cliente)
                    root = new HomeCliente();
                else
                {
                    root = new HomeLojista();
                }
            }

            MainPage = new NavigationPage(root)
            {
                BackgroundColor = (Color)Current.Resources["CinzaClaro"],
                BarBackgroundColor = (Color)Current.Resources["AzulPadrao"],
                BarTextColor = (Color)Current.Resources["CinzaClaro"]
            };

            AppCenter.Start("android=26f38d4c-bcda-486a-866e-aa35c1607c1d;", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
