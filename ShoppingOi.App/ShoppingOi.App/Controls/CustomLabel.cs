﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ShoppingOi.App.Controls
{
    public class CustomLabel : Label
    {
        public static readonly BindableProperty LineHeightProperty = BindableProperty.Create("LineHeight", typeof(float), typeof(CustomLabel), 0f);
        public static readonly BindableProperty IsUnderlinedProperty = BindableProperty.Create("IsUnderlined", typeof(bool), typeof(CustomLabel), false);

        public float LineHeight
        {
            get { return (float)GetValue(LineHeightProperty); }
            set { SetValue(LineHeightProperty, value); }
        }

        public bool IsUnderlined
        {
            get { return (bool)GetValue(IsUnderlinedProperty); }
            set { SetValue(IsUnderlinedProperty, value); }
        }
    }
}
