﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ShoppingOi.App.Controls
{
    public class CustomButton : Button
    {
        public static readonly BindableProperty TipoProperty = BindableProperty.Create("Tipo", typeof(TipoBotao), typeof(CustomButton), TipoBotao.Azul);

        public TipoBotao Tipo
        {
            get { return (TipoBotao)GetValue(TipoProperty); }
            set { SetValue(TipoProperty, value); }
        }

        public enum TipoBotao
        {
            Azul,
            Branco
        }
    }
}
