﻿namespace ShoppingOi.Service.Util
{
    public interface IConexao
    {
        bool EstaConectado();

        string GetBaseUrlApi();
    }
}
