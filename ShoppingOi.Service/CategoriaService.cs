﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingOi.Service.Util;
using ShoppingOi.DTO;
using System.Threading.Tasks;

namespace ShoppingOi.Service
{
    public class CategoriaService : BaseService
    {
        public CategoriaService(IConexao conexao) : base(conexao) { }

        public async Task<BaseApiClass> BuscaLista()
        {
            return await Get<CategoriaAppCollection>("/categoria", "AppCategoriaBuscaLista");
        }
    }
}
