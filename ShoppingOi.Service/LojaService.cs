﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingOi.Service.Util;
using ShoppingOi.DTO;
using System.Threading.Tasks;

namespace ShoppingOi.Service
{
    public class LojaService : BaseService
    {
        public LojaService(IConexao conexao) : base(conexao) { }

        public async Task<BaseApiClass> BuscaLista()
        {
            return await Get<LojaAppCollection>("/loja", "AppLojaBuscaLista");
        }

        public async Task<BaseApiClass> BuscaItem(string codigo)
        {
            return await Get<LojaApp>($"/loja?id={Escape(codigo)}", "AppLojaBuscaItem");
        }

        public async Task<BaseApiClass> Salvar(LojaApp loja)
        {
            return await Post<LojaApp>("/loja", loja, "AppLojaSalvar");
        }
    }
}