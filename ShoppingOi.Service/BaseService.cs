using System;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingOi.DTO;
using ShoppingOi.Service.Util;

namespace ShoppingOi.Service
{
    public class BaseService
    {
        private IConexao conexao;

        public BaseService(IConexao conexao)
        {
            this.conexao = conexao;
        }

        public async Task<BaseApiClass> Get<T>(string caminhoApi, string local) where T : BaseApiClass
        {
            return await Executa<T>(TipoMetodo.GET, caminhoApi, null, local);
        }

        public async Task<BaseApiClass> Post<T>(string caminhoApi, object item, string local) where T : BaseApiClass
        {
            return await Executa<T>(TipoMetodo.POST, caminhoApi, item, local);
        }

        public async Task<BaseApiClass> Put<T>(string caminhoApi, object item, string local) where T : BaseApiClass
        {
            return await Executa<T>(TipoMetodo.PUT, caminhoApi, item, local);
        }

        private async Task<BaseApiClass> Executa<T>(TipoMetodo tipo, string caminhoApi, object item, string local) where T : BaseApiClass
        {
            try
            {
                if (!EstaConectado())
                    return BaseApiClass.TelefoneNaoConectado;

                var uri = new Uri(string.Format(conexao.GetBaseUrlApi(), caminhoApi));

                StringContent stringContent = null;

                if (tipo != TipoMetodo.GET)
                    stringContent = CriaStringContent(item);

                HttpResponseMessage response;

                using (var httpClient = new HttpClient())
                {
                    switch (tipo)
                    {
                        case TipoMetodo.GET:
                            response = await httpClient.GetAsync(uri);
                            break;
                        case TipoMetodo.POST:
                            response = await httpClient.PostAsync(uri, stringContent);
                            break;
                        case TipoMetodo.PUT:
                            response = await httpClient.PutAsync(uri, stringContent);
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var baseClass = JsonConvert.DeserializeObject<BaseApiClass>(content);

                    if (baseClass.Sucesso)
                        return JsonConvert.DeserializeObject<T>(content);
                    else
                        return baseClass;
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                        return BaseApiClass.MontaErro(BaseApiClass.CodigoRetornoEnum.MetodoNaoEncontrado, null);

                    throw new Exception(response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                if (!(item is ErroApp))
                    EnviaErro(local, ex.ToString());

                return BaseApiClass.MontaErro(ex.Message);
            }
        }

        protected StringContent CriaStringContent(object item)
        {
            return new StringContent(JsonConvert.SerializeObject(item), Encoding.Default, "application/json");
        }

        protected async void EnviaErro(string local, string mensagem)
        {
            await new ErroService(conexao).Inserir(new ErroApp
            {
                Local = local,
                Mensagem = mensagem
            });
        }

        protected bool EstaConectado()
        {
            return conexao.EstaConectado();
        }

        protected string Escape(string valor)
        {
            return Uri.EscapeDataString(valor);
        }

        public enum TipoMetodo
        {
            GET,
            POST,
            PUT
        }
    }
}