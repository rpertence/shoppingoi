﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingOi.Service.Util;
using ShoppingOi.DTO;
using System.Threading.Tasks;

namespace ShoppingOi.Service
{
    public class UsuarioService : BaseService
    {
        public UsuarioService(IConexao conexao) : base(conexao) { }

        public async Task<BaseApiClass> Criar(UsuarioApp item)
        {
            return await Post<UsuarioApp>("/usuario", item, "AppUsuarioCriar");
        }

        public async Task<BaseApiClass> Login(string email, string senha)
        {
            return await Get<UsuarioApp>($"/usuario/login?email={email}&senha={senha}", "AppUsuarioLogin");
        }

        public async Task<BaseApiClass> EnviarCodigoRecuperacaoSenha(string email)
        {
            return await Get<BaseApiClass>($"/usuario/enviarcodigoredefinicaosenha?email={email}", "AppUsuarioEnviarCodigoRecuperacaoSenha");
        }

        public async Task<BaseApiClass> ConfirmarCodigo(string email, string codigoConfirmacao)
        {
            return await Get<UsuarioApp>($"/usuario/confirmarcodigo?email={email}&codigoConfirmacao={codigoConfirmacao}", "AppUsuarioConfirmarCodigo");
        }

        public async Task<BaseApiClass> AlterarSenha(UsuarioApp item)
        {
            return await Put<UsuarioApp>("/usuario/alterarsenha", item, "AppUsuarioAlterarSenha");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email">E-mail de cadastro</param>
        /// <param name="tipoCodigo">1 - Validação de cadastro, 2 - Redefinição de senha</param>
        /// <returns></returns>
        public async Task<BaseApiClass> ReenviarCodigo(string email, int tipoCodigo)
        {
            return await Post<BaseApiClass>($"/usuario/reenviarcodigo?email={email}&tipoCodigo={tipoCodigo}", null, "AppUsuarioReenviarCodigo");
        }
    }
}
