using ShoppingOi.DTO;
using ShoppingOi.Service.Util;
using System.Threading.Tasks;

namespace ShoppingOi.Service
{
    public class ErroService : BaseService
    {
        public ErroService(IConexao conexao)
            : base(conexao) { }

        public async Task Inserir(ErroApp item)
        {
            await Post<ErroApp>("/erro", item, "ApiErroInserir");
        }
    }
}