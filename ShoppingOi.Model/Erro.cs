﻿using RafaelPertence.ERP.Data.Interface;
using RafaelPertence.ERP.Model;
using RafaelPertence.ERP.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.Model
{
    public class Erro : Base<Erro, IRepositorioBase<Erro>>
    {
        [Campo(true)]
        public uint Codigo { get; set; }

        [Campo]
        public string Local { get; set; }

        [Campo]
        public string Mensagem { get; set; }

        [Campo]
        public DateTime Data { get; set; }

        public void Inserir()
        {
            try
            {
                Data = DateTime.Now;
                Salvar();
            }
            catch { }
        }

        public static void Inserir(string local, string mensagem)
        {
            new Erro
            {
                Local = local,
                Mensagem = mensagem
            }.Inserir();
        }
    }
}
