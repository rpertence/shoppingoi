﻿using RafaelPertence.ERP.Data.Interface;
using RafaelPertence.ERP.Model;
using RafaelPertence.ERP.Util;
using System.Collections.Generic;

namespace ShoppingOi.Model
{
    public class LojaCategoria : Base<LojaCategoria, IRepositorioBase<LojaCategoria>>
    {
        [Campo]
        public uint CodigoLoja { get; set; }

        [Campo]
        public uint CodigoCategoria { get; set; }

        private Categoria categoria;
        public Categoria Categoria
        {
            get
            {
                if (categoria == null)
                    categoria = Categoria.GetById(this.CodigoCategoria);

                return categoria;
            }
        }

        public static void ExcluirFromLoja(uint codigoLoja)
        {
            var repo = GetRepository();
            
            var itens = repo.BuscaLista(Filtro.FiltroInt32Igual("CodigoLoja", codigoLoja));

            foreach (var item in itens)
            {
                repo.Excluir(new Filtro
                {
                    Filtros = new List<Filtro>
                    {
                        Filtro.FiltroInt32Igual("CodigoLoja", codigoLoja),
                        Filtro.FiltroInt32Igual("CodigoCategoria", item.CodigoCategoria)
                    }
                });
            }
        }
    }
}