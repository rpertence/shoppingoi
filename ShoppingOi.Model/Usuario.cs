﻿using RafaelPertence.ERP.Data.Interface;
using RafaelPertence.ERP.Model;
using RafaelPertence.ERP.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.Model
{
    public class Usuario : Base<Usuario, IRepositorioBase<Usuario>>
    {
        [Campo(true)]
        public uint Codigo { get; set; }

        [Campo]
        public string Nome { get; set; }

        [Campo]
        public string Email { get; set; }

        [Campo]
        public string Senha { get; set; }

        [Campo]
        public bool Ativo { get; set; }

        [Campo]
        public DateTime DataCadastro { get; set; }

        [Campo]
        public string CodigoValidacao { get; set; }

        [Campo]
        public DateTime DataValidadeCodigo { get; set; }

        private Loja loja;
        public Loja Loja
        {
            get
            {
                if (loja == null)
                    loja = Loja.GetRepository().BuscaItem(Filtro.FiltroInt32Igual("CodigoUsuario", Codigo));

                return loja;
            }
        }

        public static bool ExisteEmail(string email)
        {
            return ExisteEmail(email, null);
        }

        public static bool ExisteEmail(string email, uint? codigo)
        {
            Usuario usuario = GetRepository().BuscaItem(Filtro.FiltroStringIgual("Email", email));

            if (usuario == null) return false;
            else if (codigo.HasValue) return usuario.Codigo != codigo.Value;
            else return true;
        }
    }
}
