﻿using RafaelPertence.ERP.Data.Interface;
using RafaelPertence.ERP.Model;
using RafaelPertence.ERP.Util;

namespace ShoppingOi.Model
{
    public class Categoria : Base<Categoria, IRepositorioBase<Categoria>>
    {
        [Campo(true)]
        public uint Codigo { get; set; }

        [Campo]
        public string Descricao { get; set; }

        [Campo]
        public bool Ativo { get; set; }
    }
}