﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.Model
{
    public class Produto
    {
        public uint Codigo { get; set; }

        public uint CodigoLoja { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

        public decimal Preco { get; set; }

        public decimal? PrecoComDesconto { get; set; }

        public uint Ordem { get; set; }

        public bool Ativo { get; set; }
    }
}
