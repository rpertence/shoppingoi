﻿using RafaelPertence.ERP.Data.Interface;
using RafaelPertence.ERP.Model;
using RafaelPertence.ERP.Util;
using ShoppingOi.Data.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ShoppingOi.Model
{
    public class Loja : Base<Loja, ILojaRepositorio<Loja>>
    {
        [Campo(true)]
        public uint Codigo { get; set; }

        [Campo]
        public uint CodigoUsuario { get; set; }

        [Campo]
        public string Nome { get; set; }

        [Campo]
        public string Descricao { get; set; }

        [Campo]
        public string Localizacao { get; set; }

        public string Categoria { get; set; }

        private List<LojaCategoria> lojasCategorias;
        public List<LojaCategoria> LojasCategorias
        {
            get
            {
                if (lojasCategorias == null)
                    lojasCategorias = LojaCategoria.GetRepository().BuscaLista(Filtro.FiltroInt32Igual("CodigoLoja", this.Codigo));

                return lojasCategorias;
            }
        }

        private List<Categoria> categorias;
        public List<Categoria> Categorias
        {
            get
            {
                if (categorias == null)
                    categorias = LojasCategorias.Select(lc => lc.Categoria).ToList();
                return categorias;
            }
            set { categorias = value; }
        }

        public override uint Salvar()
        {
            using (var ts = new TransactionScope())
            {
                if (this.Codigo != 0)
                    LojaCategoria.ExcluirFromLoja(this.Codigo);

                base.Salvar();

                foreach (var c in Categorias)
                {
                    new LojaCategoria
                    {
                        CodigoCategoria = c.Codigo,
                        CodigoLoja = this.Codigo
                    }.Salvar();
                }

                ts.Complete();

                return Codigo;
            }
        }
    }
}