﻿using RafaelPertence.ERP.Data;
using RafaelPertence.ERP.Data.OAD;
using ShoppingOi.Data.Interface;
using ShoppingOi.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.Data
{
    public class LojaRepositorio : RepositorioBase<Loja>, ILojaRepositorio<Loja>
    {
        public List<Loja> BuscaListaLojas()
        {
            string sql = @"select l.codigo, l.nome, c.descricao categoria
from lojacategoria lc
  inner
join loja l on lc.codigoloja = l.codigo
inner join categoria c on lc.codigocategoria = c.codigo";

            var dt = new Database().DataTablePorStringSQL(sql);

            var lista = new List<Loja>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var row = dt.Rows[i];

                var loja = new Loja
                {
                    Codigo = row.Field<uint>("Codigo"),
                    Nome = row.Field<string>("Nome"),
                    Categoria = row.Field<string>("categoria")
                };

                lista.Add(loja);
            }

            return lista;
        }
    }
}
