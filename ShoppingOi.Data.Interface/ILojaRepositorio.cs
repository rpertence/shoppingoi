﻿using RafaelPertence.ERP.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.Data.Interface
{
    public interface ILojaRepositorio<T> : IRepositorioBase<T>
    {
        List<T> BuscaListaLojas();
    }
}
