﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.DTO
{
    public class LojaApp : BaseApiClass
    {
        public string Codigo { get; set; }

        public string CodigoUsuario { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

        public string Localizacao { get; set; }

        public string Categoria { get; set; }

        public List<CategoriaApp> Categorias { get; set; }

        public List<LojaCategoriaApp> LojasCategorias { get; set; }
    }

    public class LojaAppCollection : BaseApiClass
    {
        public IEnumerable<LojaApp> Lojas { get; set; }
    }
}
