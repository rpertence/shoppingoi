﻿//using Android.Runtime;
using System.Collections.Generic;

namespace ShoppingOi.DTO
{
    //[Preserve(AllMembers = true)]
    public class CategoriaApp
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }

        //public bool Selecionada { get; set; }
    }

    //[Preserve(AllMembers = true)]
    public class CategoriaAppCollection : BaseApiClass
    {
        public IEnumerable<CategoriaApp> Categorias { get; set; }
    }
}
