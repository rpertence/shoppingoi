﻿//using Android.Runtime;

namespace ShoppingOi.DTO
{
    //[Preserve(AllMembers = true)]
    public class BaseApiClass
    {
        public BaseApiClass()
        {
            this.Sucesso = true;
        }

        public static BaseApiClass BaseSucesso = new BaseApiClass { Sucesso = true };

        public static BaseApiClass Erro = new BaseApiClass { Sucesso = false };

        public static BaseApiClass TelefoneNaoConectado = new BaseApiClass
        {
            Sucesso = false,
            CodigoRetorno = BaseApiClass.CodigoRetornoEnum.TelefoneNaoConectado,
            Mensagem = "Não foi identificada conexão com a internet.\nConecte-se e tente novamente"
        };

        public bool Sucesso { get; set; }

        public CodigoRetornoEnum CodigoRetorno { get; set; }

        public string Mensagem { get; set; }

        public bool ErroInserido { get; set; }

        public static BaseApiClass MontaErro(string mensagem)
        {
            return MontaErro(0, mensagem);
        }

        public static BaseApiClass MontaErro(CodigoRetornoEnum codigo, string mensagem)
        {
            return MontaErro(codigo, mensagem, false);
        }

        public static BaseApiClass MontaErro(CodigoRetornoEnum codigo, string mensagem, bool erroInserido)
        {
            return new BaseApiClass
            {
                Sucesso = false,
                ErroInserido = erroInserido,
                CodigoRetorno = codigo,
                Mensagem = mensagem
            };
        }

        public enum CodigoRetornoEnum
        {
            Padrao = 0,
            DadosNaoEncontrados,
            EmailJaCadastrado,
            TelefoneNaoConectado,
            MetodoNaoEncontrado,
            CadastroNaoConfirmado,
            ErroEnvioEmailCadastro
        }
    }
}
