﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.DTO
{
    public class LojaCategoriaApp
    {
        public string CodigoLoja { get; set; }

        public string CodigoCategoria { get; set; }
    }
}
