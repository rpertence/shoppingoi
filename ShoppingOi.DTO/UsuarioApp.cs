﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingOi.DTO
{
    public class UsuarioApp : BaseApiClass
    {
        public string Codigo { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataCadastro { get; set; }

        public string CodigoLoja { get; set; }
    }
}
